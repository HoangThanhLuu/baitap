import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header'; // Correct import for Header component
import "./index.css";

function App() {
    return (
        <div>
            <Header /> {/* Render the Header component */}
        </div>
    );
}

export default App
