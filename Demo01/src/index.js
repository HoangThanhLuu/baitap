import React from "react"; // nạp thư viện react
import ReactDOM from "react-dom"; // nạp thư viện react-dom
import App from "./App";
import reportWebVitals from './reportWebVitals';
// Tạo component App
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
// Render component App vào #root element react 17
// ReactDOM.render(<App />, document.getElementById('root'))

//react18@
// const root = ReactDOM.createRoot(document.getElementById('root'))
// root.render(<App/>)

reportWebVitals();
